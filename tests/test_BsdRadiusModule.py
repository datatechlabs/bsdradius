"""
Test bsdradius/BsdRadiusModule.py
"""


import unittest
import sys

sys.path.insert(0, '../')

from bsdradius.BsdRadiusModule import BsdRadiusModule

class BsdRadiusModuleTestCase(unittest.TestCase):
	"""Test BsdRadiusModule class"""
	def testCreateClassInstance(self):
		"""Test BsdRadiusModule against stupid syntax or typo errors"""
		mod = BsdRadiusModule()
		str(mod)

def makeSuite():
	"""Collect test cases into suite"""
	return unittest.defaultTestLoader.loadTestsFromTestCase(BsdRadiusModuleTestCase)
